# Scheduler Plugin 

[![pipeline status](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/badges/master/pipeline.svg)](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/commits/master)

## What is this?

This is a plugin for [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB) that allows you to schedule some actions (load profile, turn off)

## Downloads

* [Windows 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Windows%2032)
* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Windows%2064)
* [Linux 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Linux%2032)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=Linux%2064)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=MacOS%20ARM64)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin/-/jobs/artifacts/master/download?job=MacOS%20Intel)

## How do I install it?

* Download and extract the correct files depending on your system
* Launch OpenRGB
* From the Settings -> Plugins menu, click the "Install plugin" button
