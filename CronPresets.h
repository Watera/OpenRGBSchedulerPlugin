#ifndef CRONPRESETS_H
#define CRONPRESETS_H

#include <string>
#include <map>

static const std::map<std::string, std::string> CRON_PRESETS = {
    {"Every second","* * * * * ?"},
    {"Every minute", "0 * * * * ?"},
    {"Every hour", "* 0 * * * ?"},
    {"Every Weekday at noon", "0 0 12 * * MON-FRI"},
    {"Every 2 days, starting on the 1st at noon", "0 0 12 1/2 * ?"},
    {"Every twelve hours", "0 0 */12 ? * *"},
    {"Every day at 8.30am", "0 30 8 * * ?"},
    {"Every day at 10pm", "0 0 22 * * ?"},
};

#endif // CRONPRESETS_H
